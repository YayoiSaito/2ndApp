#このファイルを実行することで処理が開始

from bottle import Bottle, route, run, request, post, template, static_file, redirect
from bottle_flash2 import FlashPlugin
import sqlite3
import datetime

'''CSSを使うときなどの静的ファイルをbottleで扱う為の設定
#参考サイト：https://tmg0525.hatenadiary.jp/entry/2018/03/04/004706'''
import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_DIR = os.path.join(BASE_DIR, 'static')

@route('/static/<filePath:path>')
def send_static(filePath):
    #静的ファイルを返す
    return static_file(filePath, root='./static')
#Flash Setup
app=Bottle()
COOKIE_SECRET ='super_secret_string'
app.install(FlashPlugin(secret=COOKIE_SECRET))

########DB#############################
dbname = '2ndApp.db'
conn = sqlite3.connect(dbname)#接続
c = conn.cursor()

########顧客側################################
#routはURL
@route('/')
def welcome():
    #アクセス時、DBのSeatsテーブルに一週間後までの日付があるか確認、無ければ挿入する
    today = datetime.date.today() #今日(yyyy-mm-dd形式)
    conn = sqlite3.connect(dbname)#接続
    c = conn.cursor()

    for i in range(7): #i = 0~6まで、iを1ずつ増やしながら繰り返す
      day = today + datetime.timedelta(days=i) #今日 + i日で今日から6日後までを表す
      day_str = day.strftime("%Y/%m/%d")  #yyyy-mm-dd形式⇒yyyy//mm/dd

      #Daysテーブル(Seatsテーブルに空席状況を表した行がある日付のみを保持するテーブル)に
      #day_strがあるかを確認（⇒Seatsテーブルにその日を追加する必要があるかを判断する）
      query = "select date from Days where date= '" + day_str+"'"
      result = c.execute(query).fetchone()
      #day_strがDaysテーブルにあるか確認
      if result is None: #もし無ければ、add_Seatsへ
         add_Seats(day)

    c.close()
    conn.close()
    return template('index',app=app)

def add_Seats(day):
    #引数で渡された日のSeatsテーブルとDaysテーブルを挿入
    conn = sqlite3.connect(dbname)#接続
    c = conn.cursor()
    time_val = datetime.datetime.combine(day, datetime.time(10, 0)) # dayの10:00を取得
    day_str = day.strftime("%Y/%m/%d")

    while time_val.time() <= datetime.time(17, 0):
      time_str = time_val.strftime("%H:%M:%S") # 今time_valに入っている値を「00：00」のフォーマットで文字列にして受け取る
      last_seat = 30
      query = "INSERT INTO Seats (date, time, last_seat) VALUES ('" + day_str + "','" + time_str + "'," +str(last_seat)+")"
      c.execute(query)

      c.close()
      conn.commit()
      c = conn.cursor()##移動

      time_val += datetime.timedelta(minutes=30)

    insert_Days ="INSERT INTO Days (date) VALUES('" +day_str+ "')"
    c.execute(insert_Days)
    c.close()
    conn.commit()
    conn.close()


#店内飲食の場合。席予約を取る
@route('/seatReservation')
def seat_reservation():
    # テンプレートを読み込み、データを適用してHTMLとしてレンダリング
    return template('seatReservation')


#テイクアウトの場合。受け取り予約を取る
@route('/takeoutReservation')
def takeout_reservation():
    return template('takeoutReservation')

@route('/checkAvailability', method='POST')
def check_availability():
    # POSTリクエストからdateとtimeを取得
    date = request.forms.get('date')
    time = request.forms.get('time')
    
    if date=="" or time=="":
      content="""  
      <h2>エラー</>
      <p>日時を選択して、再度お試し下さい。</p>
      <input  type="button" class="back" onclick="history.back()" value="戻る">
      """
      return template(content,)


    time_str = str(time)+ ":00"
    here_or_togo = request.forms.getunicode('here_or_togo')
    # ここでdateとtimeを使ってDB処理を行う
    conn = sqlite3.connect(dbname)
    c = conn.cursor() #カーソルオブジェクトをつくる

    query = "SELECT last_seat FROM Seats WHERE date='" +date+ "'and time='" +time_str+ "'"
    last_seats = c.execute(query).fetchone()

    conn.close()
    last_seats=int(last_seats[0])



    # DB処理結果に応じて予約可能かどうかのデータを返す
    if last_seats >= 1:
        # 予約可能な場合の画面
        content="""
        <p>予約可能です</p>
        <form action="/menu" method="post">
          <input type="submit" value="この日時で予約する">
          <input type = "hidden" name = "date" value='{{date}}'>
          <input type = "hidden" name = "time" value='{{time}}'>
				  <input type="hidden" name="here_or_togo" value='{{here_or_togo}}'>
        </form>
        """
        return template(content, date=date, time=time, here_or_togo=here_or_togo)
    else:
        # 満席などの場合の画面
        content="""
        <p>予約できません。他の日時で再度お試し下さい。</p>
        <input  type="button" class="back" onclick="history.back()" value="戻る">

        """
        return template(content)

#メニュー一覧を表示、選択
@route('/menu', method='POST')
def menu():
    #POSTリクエストのパラメータ受け取り
    date = request.forms.getunicode('date')
    time = request.forms.getunicode('time')
    here_or_togo = request.forms.getunicode('here_or_togo')
    
    item_list = get_item()
    category_dict = {"seasonMenus":"期間限定", "hotDrinks":"ホット", "iceDrinks":"アイス", "snacks":"軽食", "sweets":"焼き菓子"}
    return template('menu', date=date, time=time, here_or_togo=here_or_togo, item_list=item_list, category_dict=category_dict)

#7/25追加 メニュー一覧をDBから取り出すメソッド
def get_item():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    select = "select item_id, item_name, price, category from Items"
    c.execute(select)
    #selectした全メニューを,配列の辞書に受け取り(配列の一つの要素が、1商品の情報を表した辞書になる)
    # イメージは列がそれぞれitem_id,item_name,price,category、行が1メニュー
    item_list=[]
    for row in c.fetchall():#DBに入っている商品すべてに対して繰り返して処理する
        item_list.append({#item_listに辞書型の1メニューを表す要素を追加
			"item_id": row[0],
			"item_name": row[1],
      "price": row[2],
      "category": row[3]
		})
    conn.close()
    return item_list


#注文確認画面
@route('/confirm',method='POST')
def confirm():
    cupon = request.forms.getunicode('cupon')
    #DBの準備
    conn = sqlite3.connect(dbname)
    c = conn.cursor() 

    #合計を求める変数
    sum=0
    #メニュー選択をdata_listへ受け取り
    data_list = []
    for key, value in request.forms.items(): #menu.html(注文画面)から値受けとり
      #keyはitem_id、valueは注文数量。
      #以下の要素はdata_listに含めない
      if key.startswith('tab'):
        continue  # タブの選択状態
      if key.startswith('cupon'):
        continue  # クーポン
      if key.startswith('date'):
        continue  # 予約日
      if key.startswith('time'):
        continue  # 予約時間
      if key.startswith('here_or_togo'):
        continue  # 店内飲食かテイクアウトか

      
      if value != '0': 
        select_name = "select item_name from Items where item_id = '" + key + "'"
        select_price = "select price from Items where item_id = '" + key + "'"
        name=c.execute(select_name).fetchone() #('クリームソーダ',)という状態
        #nameはtuple型なので、String型へ変更⇒スライスで要らない('と',)を無くした文字列に
        name_str = str(name)
        name = name_str[2:-3] #要らない部分が削除された商品名が格納
        result = c.execute(select_price).fetchone()
        if result:
          price = int(result[0])
        else:
          price=0

        sum =sum + int(price) * int(value) #値段を入れる

        data_list.append({ #data_list配列に1注文分の辞書を追加
            "item_id": key,
            "item_name": name,
            "price": price,
            "namber": int(value)
        })
    conn.close()

    #データの引き継ぎ
    date = request.forms.getunicode('date')
    time = request.forms.getunicode('time')
    here_or_togo = int(request.forms.getunicode('here_or_togo'))

    here_or_togo_text=""
    if int(here_or_togo) == 1:
        here_or_togo_text = "テイクアウト"
    elif int(here_or_togo) == 0:
        here_or_togo_text = "店内飲食"
    else:
        here_or_togo_text= "未選択"

    return template('confirm',data_list = data_list, cupon=cupon, date=date, time=time, here_or_togo_text=here_or_togo_text, here_or_togo=here_or_togo, sum=sum)


#注文情報を出す注文番号を入力する画面
@route("/rog")
#index.htmlから遷移してきたときの動作
def rog():
   return template('rog')

#注文情報を表示
@route("/rog_result")
#rog.htmlで確認ボタンがクリックされた時の動作
def rog_show():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    #入力欄に入力された注文番号の受け取り
    reservation_id = str(request.query.reservation_id)
    #reservation_idに入った注文番号の予約情報を取り出す
    query = "SELECT * FROM Reservations WHERE reservation_id=" +reservation_id
    c.execute(query)
    #selectした全予約を,配列の辞書に受け取り(配列の一つの要素が、1商品の情報を表した辞書になる)
    #※将来的に、注文番号ではなく社員番号を利用して、その人が予約している全予約を出したいから配列にした
    reservation_data=[]

    for row in c.fetchall():#DBに入っている予約すべてに対して繰り返して処理する
      reservation_data.append({#reservation_dataに辞書型の1予約を表す要素を追加
      "reservation_id": row[0],
      "reservation_date": row[1],
      "reservation_time": row[2],
      "cupon": row[3],
      "sum" : row[4],
      "here_or_togo": row[5],
      })
    c.close()
       
    c=conn.cursor()
    query = "SELECT item_id, item_pieces FROM OrderedItems WHERE reservation_id='" +reservation_id+ "'"
    c.execute(query)
    #入力された注文番号で注文された商品の情報
    ordered=[]
    for row in c.fetchall():#DBに入っている予約すべてに対して繰り返して処理する
      ordered.append({#orderedに辞書型の1予約を表す要素を追加
      "item_id": row[0],
      "item_pieces": row[1],
      })
    c.close()

    for order in ordered: #orderedの中の辞書を一つずつorderに受け取り
      c=conn.cursor()
      select_item= "SELECT item_name, price FROM Items WHERE item_id ='" +order['item_id']+ "'"
      c.execute(select_item)
      for row in c.fetchall():
        name = row[0]
        price = row[1]
        #name = name_str[2:-3] #要らない部分が削除された商品名が格納
        
        order.update(item_name = name, price=price)
      c.close()

    conn.close()
    return template('rog_result', reservation_data = reservation_data, order_list = ordered)


@route("/cancel", method = "POST")
def cancel():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()

    reservation_id = str(request.forms.getunicode('reservation_id'))
    cancel_Items = "DELETE FROM OrderedItems WHERE reservation_id = " +reservation_id
    c.execute(cancel_Items)
    c.close()
    conn.commit()

    c = conn.cursor()
    select_here_or_togo = "SELECT here_or_togo, reservation_date, reservation_time FROM Reservations WHERE reservation_id = "+reservation_id
    li = c.execute(select_here_or_togo).fetchone()
    c.close()

    #もし店内飲食だったらSeatsテーブルを更新する
    if li[0] == 0:
      c=conn.cursor()
      update_seats = "UPDATE Seats SET last_seat = last_seat+1 WHERE date  = '" +li[1]+ "' and time='" +li[2]+ "'"
      c.execute(update_seats)
      c.close()
      conn.commit()

    
    c = conn.cursor()
    cancel_Reservations = "DELETE FROM Reservations WHERE reservation_id = " +reservation_id
    c.execute(cancel_Reservations)
    c.close()

    conn.commit()
    conn.close()

    return template("cancel", canceled_id = reservation_id)



########店員側######################################
@route('/reservationView')
def reservationView():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    today = datetime.date.today() #今日の日付を取得
    today_str = today.strftime("%Y/%m/%d")

    query = "SELECT * FROM Reservations WHERE '" +today_str+ "' <= reservation_date ORDER BY reservation_date, reservation_time"
    c.execute(query)
    #selectした全予約を,配列の辞書に受け取り(配列の一つの要素が、1商品の情報を表した辞書になる)
    order_list=[]

    for row in c.fetchall():#DBに入っている予約すべてに対して繰り返して処理する
      order_list.append({#order_listに辞書型の1予約を表す要素を追加
      "reservation_id": row[0],
      "reservation_date": row[1],
      "reservation_time": row[2],
      "cupon": row[3],
      "sum" : row[4],
      "here_or_togo": row[5],
      "come": row[6],
      "finish": row[7],
      "employee_id": row[8]
      })
    conn.close()

    return template('reservationView', order_list = order_list)

#「対応する」ボタンを押した時の処理
#作成途中です。現状は未対応と完了の2つの状況しか表現できませんが、
#「対応する」ボタンを押すとそのメニュー一覧を表示し、他のユーザには「対応中」と表示します。
@route("/take/<reservation_id:int>")
def take(reservation_id):
    update_finish(reservation_id)

    return redirect("/reservationView")

def update_finish(reservation_id):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    #ボタンをクリックしたら、Reservationsテーブルを更新
    update_finish = "UPDATE Reservations SET finish=2 WHERE reservation_id=?"
    c.execute(update_finish, (reservation_id,))
    conn.commit()
    conn.close()
    
#「到着した」ボタンクリック時
@route("/come/<reservation_id:int>")
def cometake(reservation_id):
   update_come(reservation_id)
   return redirect("/reservationView")

def update_come(reservation_id):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    #ボタンをクリックしたら、Reservationsテーブルを更新
    update_come = "UPDATE Reservations SET come=1 WHERE reservation_id=?"
    c.execute(update_come, (reservation_id,))
    conn.commit()
    conn.close()




if __name__ == "__main__":
    run(host='localhost', port=8000, reload=True ,debug=True)

