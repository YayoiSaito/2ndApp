**運用上の注意**

1. **サーバーでdatabase.pyを実行してから運用すること**

> 本アプリのデータベースは、database.pyの中に書かれているコードにより生成されます。
> その為、database.pyを実行せずにApp.pyを実行すると、データベースへのクエリを実行した時点でエラーが発生します。

2. **database.pyを再実行すると、全てのデータがリセットされる**

> 前述のとおり、database.pyはデータベース生成プログラムです。
> 再実行すると、全てのテーブルをDROPして作り直す仕様になっている関係上、予約情報も含めて全てのデータが消失します。
> 安易に再実行しないでください。

3. **サーバーでApp.pyを実行してから運用すること**

> App.pyが実行されることで、ルートディレクトリを含め、全てのページがルーティングされます。
> App.pyが実行されていない状態でユーザーが本WebアプリのURLにアクセスすると「404 Not Found」エラーが発生します。

4. 	**メニュー選択画面の「おすすめ情報」タブについて**
> 「おすすめ情報」タブ下に、店員が更新するSNSや社内掲示板、ポータルサイトなどのURLをHTMLのaタグ等で埋め込むことが必要です。(今回はURLが分からないため、埋め込めませんでした。)
> 店員のタイムリーな発信を顧客が閲覧できるようにすることで、顧客単価向上に繋げます。

詳しくは[ドキュメント](https://dtintern.sharepoint.com/:w:/s/msteams_57643c_398660/EeEk1whC58NEnW4TQaF7HagBz0X0AGEHV0D4hh30EaSl8w?e=0nHIAO)をご覧下さい。
