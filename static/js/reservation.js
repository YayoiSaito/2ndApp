$(document).ready(function(){
	// 日付選択ボタンクリック時の処理
	$('#datepicker_button').click(function(){
		$('#datetimepicker').datetimepicker('show'); //Date Time Picker表示
	});

	/*var now = new Date();
	var hour = now.getHours();
	var min = now.getMinutes();
	var now_time = hour +":"+ min;

	var y = now.getFullYear();
  var m = ("00" + (now.getMonth()+1)).slice(-2);
  var d = ("00" + now.getDate()).slice(-2);
  var today = y + "/" + m + "/" + d;
	var today_date= new Date(y, m-1, d);
	console.log(y);
	console.log(m);
	console.log(d);*/


	// 日付テキストボックス・時刻テキストボックスへ設定
	$('#datetimepicker').datetimepicker({
		format:'Y/m/d H:i ',
		step:30,
		minDate:'+1970/01/02',
		maxDate:'+1970/01/08',
		minTime:'10:00',
		//minTime : now_time,
		maxTime:'16:00',
	//土日は選択不可
		disabledWeekDays: [0, 6],

		/*beforeShowTime: function (date,time){
			if ( date.getYears() == y && date.getMonths() == m && date.getDays() == d){
				return [false, 'ui-state-disabled'];
			}else{}

		},*/
		onChangeDateTime:function(dp,$input){
			//日付と時間を分けて表示。
			//後ほど、この値で予約可能かDB参照する
			var datetime = $input.val().split(' ');
			$('#txtDate').val(datetime[0]);
			$('#txtTime').val(datetime[1]);
		}
	});

});
